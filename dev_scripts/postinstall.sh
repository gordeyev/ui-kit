#!/bin/sh

if [ "$(uname -s)" = 'Linux' ]; then
  BASEDIR=$(dirname "$(readlink -f "$0" || echo "$(echo "$0" | sed -e 's,\\,/,g')")")
else
  BASEDIR=$(dirname "$(readlink "$0" || echo "$(echo "$0" | sed -e 's,\\,/,g')")")
fi


cd ${BASEDIR}

cp ./pre-push ../.git/hooks/pre-push

