import * as React from 'react'

import { BrowserRouter, Route, Switch, Redirect, browserHistory } from 'react-router-dom'
import BasePage from './pages/basePage'

export const App = () => {
  return (
    <BrowserRouter>
      <div className='container'>
        <Switch>
          <Route path='/' component={BasePage} />
        </Switch>
      </div>
    </BrowserRouter>
  )
}
