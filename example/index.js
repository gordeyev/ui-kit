import React from 'react'
import ReactDOM from 'react-dom'

import './index.css' // custom styles for examples only

import { App } from './app'

ReactDOM.render(<App />, document.getElementById('app'))
