import * as React from 'react'
import { List, ListItem, ListCol } from '../../src/components/list'
import Icon from '../../src/components/icon'

const dataSource = [{
  key: 'r1',
  icon: 'default',
  cols: [{
    data: 'some text'
  }, {
    data: 'some text'
  }]
}, {
  key: 'r2',
  icon: 'arrow',
  cols: [{
    data: 'some text'
  }, {
    data: 'some text'
  }]
}, {
  key: 'r3',
  icon: 'arrow',
  cols: [{
    data: 'some text'
  }, {
    data: 'some text'
  }]
}, {
  key: 'r4',
  icon: 'arrow',
  cols: [{
    data: 'some text'
  }, {
    data: 'some text'
  }]
}, {
  key: 'r5',
  icon: 'arrow',
  cols: [{
    data: 'some text'
  }, {
    data: 'some text'
  }]
}, {
  key: 'r6',
  icon: 'arrow',
  cols: [{
    data: 'some text'
  }, {
    data: 'some text'
  }]
}, {
  key: 'r7',
  icon: 'arrow',
  cols: [{
    data: 'some text'
  }, {
    data: 'some text'
  }]
}]

let listItems = function () {
  return dataSource.map((itemData) => {
    let cols = itemData.cols.map((col, index) => {
      return (
        <ListCol xxl={5} xl={5} lg={5} key={index}>
          {col.data}
        </ListCol>
      )
    })
    return (
      <ListItem gutter={16} key={itemData.key}>
        <ListCol className='example-list-icon' xxl={2} xl={2} lg={2}>
          <Icon type='t-send' />
        </ListCol>
        {cols}
      </ListItem>
    )
  })
}

export default class ListPage extends React.Component {
  render () {
    return (
      <div>
        <h2>List</h2>
        <div>
          <List>
            {listItems()}
          </List>
        </div>
      </div>
    )
  }
}
