import React from 'react'
import {Row, Col} from 'antd'
import Form from '../../src/components/form'
import Input from '../../src/components/input'
import Icon from '../../src/components/icon'

const FormItem = Form.Item

class ExampleForm extends React.Component {
  render () {
    const { getFieldDecorator } = this.props.form
    return (
      <Form>
        <FormItem>
          {getFieldDecorator('email', {
            rules: [{
              type: 'email',
              message: 'The input is not valid E-mail!'
            }, {
              required: true,
              message: 'Please input your E-mail!'
            }],
            trigger: 'onBlur'
          })(
            <Input placeholder='example@mail.com' />
          )}
        </FormItem>
        <FormItem>
          <Input placeholder='Search' addonBefore={<Icon type='search' />} />
        </FormItem>
        <FormItem>
          <Input disabled placeholder='Disabled field' />
        </FormItem>
      </Form>
    )
  }
}

const WrappedExampleForm = Form.create()(ExampleForm)

export default class FormPage extends React.Component {
  render () {
    return (
      <div>
        <h2>Form</h2>
        <Row>
          <Col span={8}>
            <WrappedExampleForm />
          </Col>
        </Row>
      </div>
    )
  }
}
