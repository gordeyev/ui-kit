import React from 'react'
import {Row, Col} from 'antd'

let blockProps = {
  'height': '200px',
  'lineHeight': '200px',
  'textAlign': 'center',
  'color': 'white'
}

export default class InputPage extends React.Component {
  render () {
    return (
      <div>
        <section>
          <h2>Grid</h2>
          <Row>
            <Col span={4}>
              <Row>
                <Col span={16} style={{'backgroundColor': 'rgb(141, 141, 141)', ...blockProps}}>col-4</Col>
              </Row>
            </Col>
            <Col span={12} style={{'backgroundColor': 'rgb(248, 183, 0)', ...blockProps}}>col-12</Col>
          </Row>
          <Row>
            <Col span={8} style={{'backgroundColor': 'rgb(46, 168, 223)', ...blockProps}}>col-8</Col>
            <Col span={8} style={{'backgroundColor': 'rgb(78, 206, 61)', ...blockProps}}>col-8</Col>
          </Row>
        </section>
        <section style={{'paddingTop': '10px'}}>
          <h4>Gutter</h4>
          <Row gutter={32}>
            <Col span={4}>
              <Row>
                <Col span={16} style={{'backgroundColor': 'rgb(46, 168, 223)', ...blockProps}} />
              </Row>
            </Col>
            <Col span={4}>
              <Row>
                <Col span={16} style={{'backgroundColor': 'rgb(46, 168, 223)', ...blockProps}} />
              </Row>
            </Col>
            <Col span={4}>
              <Row>
                <Col span={16} style={{'backgroundColor': 'rgb(46, 168, 223)', ...blockProps}} />
              </Row>
            </Col>
            <Col span={4}>
              <Row>
                <Col span={16} style={{'backgroundColor': 'rgb(46, 168, 223)', ...blockProps}} />
              </Row>
            </Col>
          </Row>
        </section>
      </div>
    )
  }
}
