import React from 'react'
import {Col, Row} from 'antd'
import ReactMarkdown from 'react-markdown'
import Input from '../../src/components/input'
import Divider from '../../src/components/divider'
import Button from '../../src/components/button'

let md = `
| Prop      | Description                                |
------------|--------------------------------------------|
| onChange  | (value: string) => {}                      |
`

export default class InputPage extends React.Component {
  state = {
    spinning: false
  }

  onClick = () => {
    this.setState({
      spinning: !this.state.spinning
    })
  }

  render () {
    const inputProps = {
      placeholder: 'Basic usage',
      spinning: this.state.spinning
    }

    return (
      <div>
        <h2>Input</h2>
        <div>
          <div className='v-docs'>
            <ReactMarkdown source={md} />
          </div>
          <Divider>Basic Example</Divider>
          <Row gutter={16}>
            <Col span={8}>
              <Input {...inputProps} />
            </Col>
            <Col span={7}>
              <Button onClick={this.onClick} type='primary'>{this.state.spinning ? 'Stop spin' : 'Spin'}</Button>
            </Col>
          </Row>
        </div>
      </div>
    )
  }
}
