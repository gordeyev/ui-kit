import React from 'react'
import {Row} from 'antd/es/grid'
import SuggestAutoComplete from '../../src/components/suggestAutoComplete'
import SuggestMultiActions from '../../src/components/suggestMultiActions'

const data = [{
  title: 'Apple',
  options: [{
    text: 'iOS',
    id: 'ios'
  }, {
    text: 'macOS',
    id: 'macos'
  }, {
    text: 'watchOS',
    id: 'watchos'
  }]
}, {
  title: 'Google',
  options: [{
    text: 'Android',
    id: 'android'
  }, {
    text: 'Chrome OS',
    id: 'chromeos'
  }]
}]

export default class ContextMenuPage extends React.Component {
  onChange = (value) => {
    console.log('on change')
    console.log(value)
  }

  onClear = () => {
    console.log('on clear')
  }

  onSuggestSelected = (value) => {
    console.log('on suggest selected')
    console.log(value)
  }

  render () {
    const customProps = {
      data: data,
      noMatchesText: 'No matches',
      onChange: this.onChange,
      onClear: this.onClear,
      onSuggestSelected: this.onSuggestSelected
    }

    return (
      <div>
        <h2>Suggest</h2>
        <Row>
          <SuggestAutoComplete {...customProps} placeholder='auto complete' />
        </Row>
        <Row style={{marginTop: '15px'}}>
          <SuggestMultiActions {...customProps} placeholder='multi actions' />
        </Row>
      </div>
    )
  }
}
