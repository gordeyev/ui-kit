import React from 'react'
import Button from '../../src/components/button'
import Badge from '../../src/components/badge'

export default class BadgePage extends React.Component {
  render () {
    return (
      <div>
        <h2>Badge</h2>
        <div>
          <Badge count={1}>
            <Button>Badge 1</Button>
          </Badge>
          <Badge count={100}>
            <Button style={{marginLeft: '50px'}} type='primary'>Badge 100</Button>
          </Badge>
        </div>
      </div>
    )
  }
}
