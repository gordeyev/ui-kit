import React from 'react'
import Spin from '../../src/components/spin'

export default class SpinPage extends React.Component {
  render () {
    return (
      <div>
        <h2>Spin </h2>
        <div style={{width: '200px', height: '100px', lineHeight: '100px', border: '1px solid yellow', textAlign: 'center'}}>
          <Spin />
        </div>
      </div>
    )
  }
}
