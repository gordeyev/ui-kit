import * as React from 'react'
import SidePanel from '../../src/components/sidePanel'
import Button from '../../src/components/button'
import ReactMarkdown from 'react-markdown'

let md = `
| Prop    | Description                                |
----------|--------------------------------------------|
| opened  | Whether the modal dialog is visible or not |
`

export default class SidePanelPage extends React.Component {
  state = {
    visible: false
  }

  showModal = () => {
    this.setState({
      visible: true
    })
  }

  handleCancel = () => {
    this.setState({
      visible: false
    })
  }

  render () {
    let props = {
      visible: this.state.visible,
      mainTitle: 'Сжигание токенов 5000 VST',
      subTitle: 'GVXGYSZngMsMe9MY6rzcgperu8Ry5GmRnogjRdRRD8SW',
      icon: 't-tokenburn',
      theme: 'black',
      onCancel: this.handleCancel
    }
    return (
      <div>
        <div className='v-docs'>
          <ReactMarkdown source={md} />
        </div>
        <Button type='primary' onClick={this.showModal}>Open</Button>
        <SidePanel {...props}>
          <p>some text</p>
        </SidePanel>
      </div>
    )
  }
}
