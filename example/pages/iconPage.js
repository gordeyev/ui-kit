import React from 'react'
import { Row, Col } from 'antd'
import SimpleTable from '../../src/components/simpleTable'
import Icon from '../../src/components/icon'
import { anyColorIconList, multiColorIconList } from '../../src/iconList'

let renderCellContent = function (cellData) {
  return (
    <div style={{ color: '#4ab0e0' }}>
      <Icon type={cellData.type} />
      <div>{cellData.type}</div>
    </div>
  )
}

let props = {
  'data': multiColorIconList.concat(anyColorIconList),
  'render': renderCellContent,
  'colsNumber': 4,
  'equalWidthCols': true
}

export default class IconPage extends React.Component {
  render () {
    return (
      <div>
        <h2>Icon</h2>
        <div>
          <Row>
            <Col span={16}>
              <SimpleTable {...props} />
            </Col>
          </Row>
        </div>
      </div>
    )
  }
}
