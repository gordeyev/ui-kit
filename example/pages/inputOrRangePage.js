import React from 'react'
import { Col, Row } from 'antd'
import InputOrRange from '../../src/components/inputOrRange'

export default class InputPage extends React.Component {
  onChange = (val) => {
    console.log(val)
  }

  render () {
    return (
      <div>
        <h2>Input Or Range</h2>

        <div>
          <Row>
            <Col span={8}>
              <InputOrRange onChange={this.onChange} placeholder='Type number or range'>
                <span>Input number or range</span>
              </InputOrRange>
            </Col>
          </Row>
        </div>
      </div>
    )
  }
}
