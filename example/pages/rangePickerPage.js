import * as React from 'react'
import RangePicker from '../../src/components/rangePicker'
import Button from '../../src/components/button'
import moment from 'moment'

export default class RangePickerPage extends React.Component {
  defaultInnerText = 'Период'

  onChange = (date, dateString) => {
    console.log(date, dateString)
  }

  onApplyClick = () => {}

  onResetClick = () => {}

  onCancelClick = () => {}

  render () {
    let props = {
      defaultInnerText: this.defaultInnerText,
      onChange: this.onChange,
      onApplyClick: this.onApplyClick,
      onResetClick: this.onResetClick,
      onCancelClick: this.onCancelClick,
      value: [moment('2017-12-19'), moment('2018-10-15')]
    }

    return (
      <div>
        <RangePicker {...props}>
          <Button icon='clock'>{'%INNER_TEXT%'}</Button>
        </RangePicker>
      </div>
    )
  }
}
