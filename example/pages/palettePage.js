import React from 'react'
import SimpleTable from '../../src/components/simpleTable'
import {Row, Col} from 'antd'

let palette = [{
  color: [10, 6, 6],
  title: 'Black',
  className: 'bg-black'
}, {
  color: [141, 141, 141],
  title: 'Gray',
  className: 'bg-gray'
}, {
  color: [255, 255, 255],
  title: 'White',
  className: 'bg-white'
}, {
  isEmpty: true
}, {
  isEmpty: true
}, {
  color: [74, 176, 224],
  title: 'Blue',
  className: 'bg-blue'
}, {
  color: [46, 168, 223],
  title: 'Marine',
  className: 'bg-marine'
}, {
  color: [78, 206, 61],
  title: 'Green',
  className: 'bg-success'
}, {
  color: [248, 183, 0],
  title: 'Orange',
  className: 'bg-warning'
}, {
  color: [240, 34, 43],
  title: 'Red',
  className: 'bg-error'
}]

let renderCellContent = function (cellData) {
  let cellContent
  if (cellData.isEmpty) {
    cellContent = <div className='color-block' />
  } else {
    cellContent = (
      <div className='color-block'>
        <div className='color-square-wrapper'>
          <div className={`${cellData.className} color-square`} />
        </div>
        <div className='color-code'>{cellData.color.join(' / ')}</div>
        <div className='color-title'>{cellData.title}</div>
      </div>
    )
  }
  return cellContent
}

let props = {
  'data': palette,
  'render': renderCellContent,
  'colsNumber': 5
}

export default class PalettePage extends React.Component {
  render () {
    return (
      <div>
        <h2>Palette</h2>
        <div>
          <Row>
            <Col span={12}>
              <SimpleTable {...props} />
            </Col>
          </Row>
        </div>
      </div>
    )
  }
}
