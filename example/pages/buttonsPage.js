import React from 'react'
import Button from '../../src/components/button'
// import Icon from '../../src/components/icon'

export default class ButtonsPage extends React.Component {
  render () {
    return (
      <div>
        <h2>Buttons</h2>
        <div>
          <Button style={{marginRight: '33px'}} icon='search'>Search</Button>
          <Button style={{marginRight: '33px'}}>Default</Button>
          <Button style={{marginRight: '33px'}} type='primary'>Primary</Button>
          <Button.Mini style={{marginRight: '33px'}} icon='exchange' />
          <Button icon='search' />
        </div>
        <div style={{'marginTop': '32px'}}>
          <Button style={{marginRight: '33px'}} icon='search' disabled>Search</Button>
          <Button style={{marginRight: '33px'}} disabled>Default</Button>
          <Button style={{marginRight: '33px'}} type='primary' disabled>Primary</Button>
          <Button icon='search' disabled />
        </div>
      </div>
    )
  }
}
