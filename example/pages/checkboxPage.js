import React from 'react'
import {Row, Col} from 'antd'
import Checkbox from '../../src/components/checkbox'
import Divider from '../../src/components/divider'

const CheckboxGroup = Checkbox.Group
const plainOptions = ['Apple', 'Microsoft', 'Google']
const defaultCheckedList = ['Apple', 'Google']

export default class CheckboxPage extends React.Component {
  state = {
    checkedList: defaultCheckedList,
    indeterminate: true,
    checkAll: false
  }

  onChange = (checkedList) => {
    this.setState({
      checkedList,
      indeterminate: !!checkedList.length && (checkedList.length < plainOptions.length),
      checkAll: checkedList.length === plainOptions.length
    })
  }

  onCheckAllChange = (e) => {
    this.setState({
      checkedList: e.target.checked ? plainOptions : [],
      indeterminate: false,
      checkAll: e.target.checked
    })
  }

  render () {
    let props = {
      indeterminate: this.state.indeterminate,
      onChange: this.onCheckAllChange,
      checked: this.state.checkAll
    }

    return (
      <div>
        <h2>Checkbox</h2>
        <Divider>Group example</Divider>
        <Row>
          <Col span={8}>
            <Row>
              <Checkbox {...props}>
                Check all
              </Checkbox>
            </Row>
            <Row>
              <CheckboxGroup options={plainOptions} value={this.state.checkedList} onChange={this.onChange} />
            </Row>
          </Col>
        </Row>
        <Divider>Disabled example</Divider>
        <Row>
          <Col span={8}>
            <Checkbox disabled>Disabled checkbox</Checkbox>
          </Col>
        </Row>
      </div>
    )
  }
}
