import React from 'react'
import Radio from '../../src/components/radio'

export default class RadioPage extends React.Component {
  state = {
    value: 2
  }

  onChange = (e) => {
    this.setState({
      value: e.target.value
    })
  }

  render () {
    let isFourDisabled = true
    return (
      <div>
        <h2>Radio</h2>
        <div>
          <Radio.Group onChange={this.onChange} value={this.state.value}>
            <Radio value={1}>Value One</Radio>
            <Radio value={2}>Value Two</Radio>
            <Radio value={3}>Value Three</Radio>
            <Radio value={4} disabled={isFourDisabled}>Value Four</Radio>
          </Radio.Group>
        </div>
      </div>
    )
  }
}
