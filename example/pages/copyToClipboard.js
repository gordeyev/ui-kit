import * as React from 'react'
// import SidePanel from '../../src/components/sidePanel'
import CopyToClipboard from '../../src/components/copyToClipboard'

export default class CopyToClipboardPage extends React.Component {
  render () {
    return (
        <div>
          <section>
            <h2>Copy to clipboard&nbsp;
              <a href='https://github.com/sudodoki/copy-to-clipboard#readme' target='_blank'>
                (Package)
              </a>
            </h2>
          </section>
          <section>
            <CopyToClipboard message='I am message put in clipboard!'/>
          </section>
        </div>
    )
  }
}
