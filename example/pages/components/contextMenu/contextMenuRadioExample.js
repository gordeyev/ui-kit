import React from 'react'
import Button from '../../../../src/components/button'
import ContextMenuRadio from '../../../../src/components/contextMenu/contextMenuRadio'

const menuData = {
  1: {
    text: 'Все транзакции',
    icon: 'qr'
  },
  2: {
    text: 'Токены',
    icon: 'token'
  },
  3: {
    text: 'Дата-транзакции',
    icon: 'template'
  },
  4: {
    text: 'Псевдонимы и разрешения',
    icon: 'user'
  },
  5: {
    text: 'Неподтверждённые транзакции',
    icon: 'qr'
  },
  6: {
    text: 'Пользователи',
    icon: 'user'
  }
}

export default class ContextMenuButtonsExample extends React.Component {
  state = {
    value: 1
  }

  onChange = (value) => {
    this.setState({
      value: value
    })
  }

  render () {
    const radioData = {
      value: this.state.value,
      lists: [{
        title: 'Транзакции',
        options: [{
          value: 1,
          text: menuData[1].text
        }, {
          value: 2,
          text: menuData[2].text
        }, {
          value: 3,
          text: menuData[3].text
        }, {
          value: 4,
          text: menuData[4].text
        }, {
          value: 5,
          text: menuData[5].text
        }]
      }, {
        title: 'Пользователи',
        options: [{
          value: 6,
          text: menuData[6].text
        }]
      }]
    }

    const customProps = {
      data: radioData,
      onChange: this.onChange,
      placement: 'bottomLeft'
    }

    return (
      <ContextMenuRadio {...customProps}>
        <Button icon={menuData[this.state.value].icon}>{menuData[this.state.value].text}</Button>
      </ContextMenuRadio>
    )
  }
}
