import React from 'react'
import Button from '../../../../src/components/button'
import ContextMenuCheckboxes from '../../../../src/components/contextMenu/contextMenuCheckboxes'

const checkboxData = {
  indeterminate: 'Все разрешения',
  options: {
    1: {
      text: 'Управление разрешениями'
    },
    2: {
      text: 'Торговля'
    },
    3: {
      text: 'Майнинг'
    },
    4: {
      text: 'Эмиссия токенов'
    }
  }
}

export default class ContextMenuButtonsExample extends React.Component {
  state = {}

  onChange = (values) => {
    console.log('on change')
    console.log(values)
  }

  render () {
    const customProps = {
      data: checkboxData,
      checkedList: ['2'],
      onChange: this.onChange,
      placement: 'bottomLeft'
    }

    return (
      <ContextMenuCheckboxes {...customProps}>
        <Button icon='permission'>Разрешения</Button>
      </ContextMenuCheckboxes>
    )
  }
}
