import React from 'react'
import ContextMenuButtons from '../../../../src/components/contextMenu/contextMenuButtons'
import Button from '../../../../src/components/button'

export default class ContextMenuButtonsExample extends React.Component {
  onChange = (value) => {
    console.log(value)
  }

  render () {
    const buttonList = [
      {
        text: 'Пополнить',
        value: 1
      },
      {
        text: 'Отправить',
        value: 2
      },
      {
        text: 'Обменять',
        value: 3
      }
    ]

    const customProps = {
      data: buttonList,
      placement: 'bottomLeft',
      onChange: this.onChange
    }

    return (
      <ContextMenuButtons {...customProps}>
        <Button icon='done'>Действие</Button>
      </ContextMenuButtons>
    )
  }
}
