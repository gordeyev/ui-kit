import '../../src/styles/index.less'
import '../../src/fonts'
import React from 'react'
import Layout from '../../src/components/layout'
import Icon from '../../src/components/icon'
import ButtonsPage from './buttonsPage'
import PalettePage from './palettePage'
import TabsPage from './tabsPage'
import InputPage from './inputPage'
import FormPage from './formPage'
import CheckboxPage from './checkboxPage'
import RadioPage from './radioPage'
import GridPage from './gridPage'
import ListPage from './listPage'
import TablePage from './tablePage'
import IconPage from './iconPage'
import SidePanelPage from './sidePanelPage'
import RangePickerPage from './rangePickerPage'
import ContextMenuPage from './contextMenuPage'
import CopyToClipboard from './copyToClipboard'
import InputOrRangePage from './inputOrRangePage'
import SuggestPage from './suggestPage'
import SpinPage from './spinPage'
import BadgePage from './badgePage'

import Menu from '../../src/components/menu'
import { Route, Switch, Redirect, NavLink, withRouter } from 'react-router-dom'

const {Sider, Content, Footer} = Layout

export default withRouter(props => <BasePageImpl {...props} />)

const SiderMenu = function (props) {
  return (
    <Menu mode='inline' defaultSelectedKeys={props.defaultSelectedKeys} style={{height: '100%', borderRight: 0, width: 256}}>
      <Menu.Item key='/palette'>
        <NavLink to='/palette'>
          <Icon type='diagram' /><span>Palette</span>
        </NavLink>
      </Menu.Item>

      <Menu.Item key='/'>
        <NavLink to='/'>
          <Icon type='eye' /><span>Buttons</span>
        </NavLink>
      </Menu.Item>

      <Menu.Item key='/tabs'>
        <NavLink to='/tabs'>
          <Icon type='token' /><span>Tabs</span>
        </NavLink>
      </Menu.Item>

      <Menu.Item key='/input'>
        <NavLink to='/input'>
          <Icon type='info' /><span>Input</span>
        </NavLink>
      </Menu.Item>

      <Menu.Item key='/radio'>
        <NavLink to='/radio'>
          <Icon type='settings' /><span>Radio</span>
        </NavLink>
      </Menu.Item>

      <Menu.Item key='/checkbox'>
        <NavLink to='/checkbox'>
          <Icon type='done' /><span>Checkbox</span>
        </NavLink>
      </Menu.Item>

      <Menu.Item key='/badge'>
        <NavLink to='/badge'>
          <Icon type='info' /><span>Badge</span>
        </NavLink>
      </Menu.Item>

      <Menu.Item key='/form'>
        <NavLink to='/form'>
          <Icon type='bubble' /><span>Form</span>
        </NavLink>
      </Menu.Item>

      <Menu.Item key='/grid'>
        <NavLink to='/grid'>
          <Icon type='qr' /><span>Grid</span>
        </NavLink>
      </Menu.Item>

      <Menu.ItemGroup title='Vostok Components'>
        <Menu.Item key='/list'>
          <NavLink to='/list'>
            <Icon type='details' /><span>Explorer List</span>
          </NavLink>
        </Menu.Item>
        <Menu.Item key='/icon'>
          <NavLink to='/icon'>
            <Icon type='diagram' /><span>Icon</span>
          </NavLink>
        </Menu.Item>
        <Menu.Item key='/side-panel'>
          <NavLink to='/side-panel'>
            <Icon type='to-right' /><span>Side Panel</span>
          </NavLink>
        </Menu.Item>
        <Menu.Item key='/context-menu'>
          <NavLink to='/context-menu'>
            <Icon type='copy' /><span>Context Menu</span>
          </NavLink>
        </Menu.Item>
        <Menu.Item key='/range-picker'>
          <NavLink to='/range-picker'>
            <Icon type='clock' /><span>Range Picker</span>
          </NavLink>
        </Menu.Item>

        <Menu.Item key='/copy'>
          <NavLink to='/copy'>
            <Icon type='copy' /><span>Copy To Clipboard</span>
          </NavLink>
        </Menu.Item>

        <Menu.Item key='/suggest'>
          <NavLink to='/suggest'>
            <Icon type='details' /><span>Suggest</span>
          </NavLink>
        </Menu.Item>

        <Menu.Item key='/spin'>
          <NavLink to='/spin'>
            <Icon type='exchange' /><span>Spin</span>
          </NavLink>
        </Menu.Item>

        <Menu.Item key='/range'>
          <NavLink to='/range'>
            <Icon type='info' /><span>Input or Range</span>
          </NavLink>
        </Menu.Item>
      </Menu.ItemGroup>
    </Menu>
  )
}

class BasePageImpl extends React.Component {
  constructor (props) {
    super(props)
    this.state = {
      collapsed: false
    }
  }

  toggle () {
    this.setState({
      collapsed: !this.state.collapsed
    })
  }

  render () {
    const logo = (
      <div className='logo'>
        {
          !this.state.collapsed
            ? <h2><Icon type='smartcontract' /> <span>VOSTOK UI KIT</span></h2>
            : <Icon type='heart-o' />
        }
      </div>
    )

    return (
      <Layout ismainlayout='true'>
        <Sider trigger={null} collapsible collapsed={this.state.collapsed}>
          {logo}
          <SiderMenu defaultSelectedKeys={[this.props.history.location.pathname]} />
        </Sider>
        <Layout>
          <Content style={{margin: '0 16px'}}>
            <div style={{padding: 24, background: '#fff', minHeight: 360}}>
              <Switch>
                <Route exact path='/palette' component={PalettePage} />
                <Route exact path='/tabs' component={TabsPage} />
                <Route exact path='/input' component={InputPage} />
                <Route exact path='/form' component={FormPage} />
                <Route exact path='/checkbox' component={CheckboxPage} />
                <Route exact path='/badge' component={BadgePage} />
                <Route exact path='/radio' component={RadioPage} />
                <Route exact path='/grid' component={GridPage} />
                <Route exact path='/table' component={TablePage} />
                <Route exact path='/list' component={ListPage} />
                <Route exact path='/icon' component={IconPage} />
                <Route exact path='/side-panel' component={SidePanelPage} />
                <Route exact path='/context-menu' component={ContextMenuPage} />
                <Route exact path='/range-picker' component={RangePickerPage} />
                <Route exact path='/copy' component={CopyToClipboard} />
                <Route exact path='/range' component={InputOrRangePage} />
                <Route exact path='/suggest' component={SuggestPage} />
                <Route exact path='/spin' component={SpinPage} />
                <Route exact path='/' component={ButtonsPage} />
                <Redirect from='/' exact to='/' />
              </Switch>
            </div>
          </Content>
          <Footer style={{textAlign: 'center'}}>
            Vostok UI Kit ©{(new Date()).getFullYear()}
          </Footer>
        </Layout>
      </Layout>
    )
  }
}
