import React from 'react'
import Tabs from '../../src/components/tabs'

let properties = {
  onChange: function (number) {
    console.log(`on change ${number}`)
  }
}

export default class TabsPage extends React.Component {
  render () {
    return (
      <div>
        <h2>Tabs</h2>
        <div>
          <Tabs defaultActiveKey='1' {...properties}>
            <Tabs.Pane tab='Tab' key='1' tabBarGutter={200}>Content of Tab Pane 1</Tabs.Pane>
            <Tabs.Pane tab='Tab' key='2'>Content of Tab Pane 2</Tabs.Pane>
            <Tabs.Pane tab='Tab' key='3'>Content of Tab Pane 3</Tabs.Pane>
          </Tabs>
        </div>
      </div>
    )
  }
}
