import React from 'react'
import Divider from '../../src/components/divider'
import {Row} from 'antd/es/grid'
import ContextMenuButtonsExample from './components/contextMenu/contextMenuButtonsExample'
import ContextMenuRadioExample from './components/contextMenu/contextMenuRadioExample'
import ContextMenuCheckboxesExample from './components/contextMenu/contextMenuCheckboxesExample'

export default class ContextMenuPage extends React.Component {
  render () {
    return (
      <div>
        <h2>Context Menu</h2>
        <Divider>Radio Buttons Example</Divider>
        <Row>
          <ContextMenuRadioExample />
        </Row>
        <Divider>Checkboxes Example</Divider>
        <Row>
          <ContextMenuCheckboxesExample />
        </Row>
        <Divider>Buttons Example</Divider>
        <Row>
          <ContextMenuButtonsExample />
        </Row>
      </div>
    )
  }
}
