# UI Kit
Based on [Ant Design UI Kit](https://ant.design/components/button/)


Install dependencies
```bash
npm i
```

Start example app in development mode. Open http://localhost:8080/ 
```bash
npm start
```

Build example app assets to /dist 
```bash
npm run build-example
```

## Importing & Usage

## Dependencies

## TODO
* Custom components lib
* Migrate example app to a server
 
