#!/usr/bin/env node
// build UI Kit package

const path = require('path')
const webpack = require('webpack')
const webpackConfig = require('./webpackConfig.js')
// const WebpackDevServer = require('webpack-dev-server')
// const CopyWebpackPlugin = require('copy-webpack-plugin')
// const build = require('webpack-build')

// set entry point
webpackConfig.entry = {
  main: [path.resolve(`src/components/index.tsx`)]
}

const compiler = webpack(webpackConfig)
compiler.run((err, stats) => {
  if (err) {
    console.error(err)
    process.exit(1)
  }
})
