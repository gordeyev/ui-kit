// const webpack = require('webpack')
const path = require('path')
// const HtmlWebpackPlugin = require('html-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')
// const autoprefixer = require('autoprefixer')

module.exports = {
  context: path.resolve('./src'),
  output: {
    filename: 'index.js',
    path: path.resolve('./distPackage'),
    chunkFilename: '[id].[hash].chunk.js'
  },
  resolve: {
    // Add `.ts` and `.tsx` as a resolvable extension.
    extensions: ['.ts', '.tsx', '.js']
  },
  module: {
    rules: [
      {
        test: /\.js?$/,
        exclude: /(node_modules|build\/)/,
        loader: 'babel-loader'
      },
      {
        test: /\.jsx?$/,
        exclude: /(node_modules|build\/)/,
        loader: 'babel-loader'
      },
      /* {
        test: /\.tsx?$/,
        loader: 'ts-loader'
      }, */
      {test: /\.ts$/, loader: 'ts-loader'},
      {test: /\.tsx$/, loader: 'ts-loader'},
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: 'css-loader'
        })
      },
      {
        test: /\.less$/,
        use: [
          'style-loader',
          'css-loader',
          'less-loader?{"javascriptEnabled":true,"sourceMap":true}'
        ]
      },
      {
        test: /\.svg/,
        use: {
          loader: 'svg-url-loader',
          options: {}
        }
      },
      {
        test: /\.(jpe?g|png|gif)$/i,
        loader: 'file-loader?name=/images/[name].[ext]'
      },
      {
        test: /\.(eot|svg|ttf|woff|woff2)$/,
        loader: 'file?name=/src/fonts.less/[name].[ext]'
      }
    ]
  },
  plugins: []
}
