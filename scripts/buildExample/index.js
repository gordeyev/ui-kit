#!/usr/bin/env node
// build Example app

const path = require('path')
const webpack = require('webpack')
const webpackConfig = require('./webpackConfig.js')
const WebpackDevServer = require('webpack-dev-server')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const build = require('webpack-build')

const isDevMode = !process.argv.find(e => e === '--prod')

webpackConfig.plugins.push(
  new webpack.optimize.CommonsChunkPlugin({
    name: ['common']
  })
)

// set entry point
webpackConfig.entry = {
  main: [path.resolve(`example/index.js`)],
  common: [
    'react',
    'react-dom',
    'antd'
  ]
}

if (isDevMode) {
  const port = 8080

  webpackConfig.plugins.push(
    new webpack.HotModuleReplacementPlugin({
      // Options...
    })
  )

  webpackConfig.entry.main.unshift(
    `webpack-dev-server/client?http://localhost:${port}/`,
    'webpack/hot/dev-server'
  )

  const compiler = webpack(webpackConfig)

  const server = new WebpackDevServer(compiler, {
    hot: true,
    historyApiFallback: true,
    stats: {
      colors: true,
      chunks: false
    },
    watchOptions: {
      ignored: /node_modules/
    }
  })

  server.listen(port)
} else {
  const compiler = webpack(webpackConfig)
  compiler.run((err) => {
    if (err) {
      console.error(err)
      process.exit(1)
    }
  })
}
