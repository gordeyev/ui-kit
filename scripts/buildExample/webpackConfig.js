const webpack = require('webpack')
const path = require('path')
const HtmlWebpackPlugin = require('html-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin')

module.exports = {
  output: {
    filename: '[name].js',
    path: path.resolve('./example/dist'),
    chunkFilename: '[id].[hash].chunk.js'
  },
  resolve: {
    // Add `.ts` and `.tsx` as a resolvable extension.
    extensions: ['.ts', '.tsx', '.js']
  },
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.js?$/,
        exclude: /(node_modules|build\/)/,
        loader: 'babel-loader'
      },
      {
        test: /\.jsx?$/,
        exclude: /(node_modules|build\/)/,
        loader: 'babel-loader'
      },
      {
        test: /\.ts$/,
        loader: 'ts-loader'
      },
      {
        test: /\.tsx$/,
        loaders: ['babel-loader', 'ts-loader'],
        exclude: [/node_modules/]

      },
      {
        test: /\.css$/,
        use: [
          'style-loader',
          'css-loader'
        ]
      },
      {
        test: /\.less$/,
        use: [
          'style-loader',
          'css-loader',
          'less-loader?{"javascriptEnabled":true,"sourceMap":true}'
        ]
      },
      {
        test: /\.(jpe?g|png|gif)$/i,
        loader: 'file-loader?name=/images/[name].[ext]'
      },
      {
        test: /\.(eot|ttf|woff|woff2)$/,
        loader: 'file-loader?name=public/fonts.less/[name].[ext]'
      },
      {
        test: /\.svg$/,
        include: path.resolve('src/icons/any_color/'),
        use: [{
          loader: 'svg-sprite-loader'
        },
        {
          loader: 'svgo-loader',
          options: {
            plugins: [
              {
                convertColors: {
                  currentColor: true
                }
              }
            ]
          }
        }]
      },
      {
        test: /\.svg$/,
        include: path.resolve('src/icons/multi_color/'),
        use: [{
          loader: 'svg-sprite-loader'
        },
        {
          loader: 'svgo-loader'
        }]
      }
    ]
  },
  plugins: [
    new HtmlWebpackPlugin({
      chunks: ['common', 'main'],
      template: path.resolve('./example/index.html')
    }),
    new ExtractTextPlugin('[name].css'),
    new webpack.optimize.CommonsChunkPlugin({
      name: 'common',
      filename: 'common.js',
      minChunks: Infinity
    })
  ]
}
