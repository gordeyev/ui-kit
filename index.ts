/* @remove-on-es-build-begin */
// this file is not used if use https://github.com/ant-design/babel-plugin-import
const ENV = process.env.NODE_ENV
if (ENV !== 'production' &&
  ENV !== 'test' &&
  typeof console !== 'undefined' &&
  console.warn &&
  typeof window !== 'undefined') {
  console.warn(
    'You are using a whole package of antd, ' +
    'please use https://www.npmjs.com/package/babel-plugin-import to reduce app bundle size.'
  )
}
/* @remove-on-es-build-end */

export { default as Button } from './src/components/button'
export { default as Checkbox } from './src/components/checkbox'
export { default as Input } from './src/components/input'
export { List, ListItem, ListCol } from './src/components/list'
export { default as SidePanel } from './src/components/sidePanel'
export { default as Icon } from './src/components/icon'
export { default as Menu } from './src/components/menu'
export { default as Radio } from './src/components/radio'
export { default as SimpleTable } from './src/components/simpleTable'
export { default as Table } from './src/components/table'
export { default as Tabs } from './src/components/tabs'
export { default as Form } from './src/components/form'
export { default as Suggest } from './src/components/suggest'
export { default as SuggestAutoComplete } from './src/components/suggestAutoComplete'
export { default as SuggestMultiActions } from './src/components/suggestMultiActions'
export { default as Spin } from './src/components/spin'
