let anyColorIconList: string[] = []
let multiColorIconList: string[] = []

function importAll (r: any, iconList: any) {
  r.keys().forEach((key: any) => {
    iconList.push({
      type: r(key).default.id
    })
  })
}

// @ts-ignore
importAll(require.context('!svg-sprite-loader!./icons/any_color', false, /.*\.svg$/), anyColorIconList)
// @ts-ignore
importAll(require.context('!svg-sprite-loader!./icons/multi_color', false, /.*\.svg$/), multiColorIconList)

export { anyColorIconList, multiColorIconList }
