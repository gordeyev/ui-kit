import * as React from 'react'
import AntPopover from 'antd/es/popover'
import classNames from 'classnames'

import Button from './button'

interface ContextMenuProps {
  overlayClassName?: string
  content: any
  children: any
  placement: any
  hasFooter?: boolean
  onApply?: (e: any) => any
  onCancel?: (e: any) => any
  onVisibleChange?: (visible: boolean) => any
  visible?: boolean
  trigger?: any
}

interface BlockProps {
  title?: string
}

class ContextMenu extends React.Component<ContextMenuProps, any> {
  static Block: any

  state = {
    visible: false
  }

  trigger: 'hover' | 'focus' | 'click' | undefined = this.props.trigger || 'click'

  constructor (props: any) {
    super(props)
  }

  hide = () => {
    this.setState({
      visible: false
    })
  }

  handleVisibleChange = (visible: boolean) => {
    this.setState({ visible })

    if (typeof this.props.onVisibleChange === 'function') {
      this.props.onVisibleChange(visible)
    }
  }

  onApply = (e: any) => {
    this.hide()

    if (typeof this.props.onApply === 'function') {
      this.props.onApply(e)
    }
  }

  onCancel = (e: any) => {
    this.hide()

    if (typeof this.props.onCancel === 'function') {
      this.props.onCancel(e)
    }
  }

  render () {
    let visible = this.state.visible
    if (this.props.visible != null) {
      visible = this.props.visible
    }

    const popoverProps = {
      ...this.props,

      visible: visible,

      trigger: this.trigger,

      onVisibleChange: this.handleVisibleChange,

      content: (
        <div>
          {this.props.content}
          {this.props.hasFooter &&
            <div className='v-context-menu-footer'>
              <Button type='primary' onMouseDown={this.onApply}>Apply</Button>
              <Button onMouseDown={this.onCancel}>Cancel</Button>
            </div>
          }
        </div>
      ),

      transitionName: 'fade', // style/core/motion/fade.less; default zoom-big

      overlayClassName: classNames(this.props.overlayClassName, 'v-context-menu')
    }

    return (
      <AntPopover {...popoverProps} />
    )
  }
}

class Block extends React.Component<BlockProps, any> {
  render () {
    return (
      <div>
        {this.props.title &&
          <div className='ant-popover-title v-context-menu-title'>{this.props.title}</div>
        }
        <div className='v-context-menu-content'>{this.props.children}</div>
      </div>
    )
  }
}

ContextMenu.Block = Block

export default ContextMenu
