import * as React from 'react'
import AntForm from 'antd/es/form'
import classNames from 'classnames'

export default class Form extends AntForm {
  static childContextTypes = AntForm.childContextTypes
  static Item = AntForm.Item
  static createFormField = AntForm.createFormField
  static create = AntForm.create

  constructor (props: any) {
    super(props)
  }

  render () {
    const customProps = {
      ...this.props,
      className: classNames(this.props.className, 'v-form')
    }

    return (
      <AntForm {...customProps} />
    )
  }
}
