import * as React from 'react'
import Suggest from './suggest'

interface SuggestAutoCompleteProps {
  onSuggestSelected: (data: any) => any
}

export default class SuggestAutoComplete extends React.Component<SuggestAutoCompleteProps, any> {
  onSuggestSelected = (data: any) => {
    if (typeof this.props.onSuggestSelected === 'function') {
      this.props.onSuggestSelected(data)
    }
  }

  render () {
    const customProps = {
      ...this.props,
      autoComplete: true,
      onSuggestSelected: this.onSuggestSelected
    }

    return (
      <Suggest {...customProps} />
    )
  }
}
