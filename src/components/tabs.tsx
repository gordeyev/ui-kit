import * as React from 'react'
import classNames from 'classnames'
import AntTabs from 'antd/es/tabs'

interface TabsProps {
  className?: string
}

export default class Tabs extends React.Component<TabsProps, any> {
  static Pane = AntTabs.TabPane

  constructor (props: any) {
    super(props)
  }

  render () {
    const customProps = {
      ...this.props,
      className: classNames(this.props.className, 'v-tabs')
    }

    return (
      <AntTabs {...customProps} />
    )
  }
}
