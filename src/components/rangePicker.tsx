import * as React from 'react'
import classNames from 'classnames'
import AntDatePicker from '../assets/date-picker'
import Button from './button'
import Moment from 'moment'

interface RangePickerProps {
  value: any
  className?: string
  dropdownClassName?: string
  wrapperClassName?: string
  open: boolean
  defaultInnerText: string
  onBlur: (e: any) => any
  onChange: (date?: Array<any>, dateString?: Array<string>) => any
  onOpenChange?: (status: boolean) => any
  onClose: () => any
  onCancelClick: (date?: Array<any>, dateString?: Array<string>) => any
  onResetClick: (date?: Array<any>, dateString?: Array<string>) => any
  onApplyClick: (date?: Array<any>, dateString?: Array<string>) => any
  children: React.ReactElement<any>
  format?: any
}

const AntRangePicker = AntDatePicker.RangePicker

export default class RangePicker extends React.Component<RangePickerProps, any> {
  format = this.props.format || 'D MMM, YYYY'

  innerTextMask = '%INNER_TEXT%'

  appliedDate = this.props.value ? [...this.props.value] : [null, null]

  state = {
    date: this.appliedDate,
    dateString: ['', ''],
    innerText: this.props.defaultInnerText,
    open: false
  }

  constructor (props: any) {
    super(props)
    if (this.props.value) {
      this.state.innerText = this.getInnerText(this.props.value)
    }
  }

  clear = () => {
    return new Promise((resolve) => {
      this.setState({
        date: [Moment(), Moment()]
      }, () => {
        this.setState({
          date: [null, null],
          dateString: ['', ''],
          innerText: this.props.defaultInnerText
        }, () => {
          this.appliedDate = this.state.date
          resolve(this.state.date)
        })
      })
    })
  }

  refreshDate = () => {
    return new Promise((resolve) => {
      this.setState({
        date: this.appliedDate
      }, () => {
        resolve(this.appliedDate)
      })
    })
  }

  getInnerText = (date: Array<any>) => {
    let innerText = this.props.defaultInnerText
    if ((date[0] != null) && (date[1] != null)) {
      innerText = `${date[0].format(this.format)} — ${date[1].format(this.format)}`
    }
    return innerText
  }

  showPicker = () => {
    if (!this.state.open) {
      this.setState({
        open: true
      })
    }
  }

  closePicker = () => {
    this.setState({
      open: false
    })
  }

  onChange = (date: Array<any>, dateString: Array<string>) => {
    this.setState({
      date: date,
      dateString: dateString
    })
  }

  onOpenChange = (status: boolean) => {
    this.refreshDate().then(() => {
      if (typeof this.props.onOpenChange === 'function') {
        this.props.onOpenChange(status)
      }
    })
  }

  afterButtonPressed = () => {
    // because we change value after button press, not after select
    if (typeof this.props.onChange === 'function') {
      this.props.onChange(this.state.date, this.state.dateString)
    }
  }

  onResetClick = () => {
    this.clear().then(() => {
      this.afterButtonPressed()

      if (typeof this.props.onResetClick === 'function') {
        this.props.onResetClick(this.state.date, this.state.dateString)
      }
    })
  }

  onCancelClick = () => {
    this.setState({
      date: this.appliedDate
    }, () => {
      this.afterButtonPressed()

      if (typeof this.props.onCancelClick === 'function') {
        this.props.onCancelClick(this.state.date, this.state.dateString)
      }
    })
  }

  onApplyClick = () => {
    this.closePicker()
    this.setState({
      innerText: this.getInnerText(this.state.date)
    }, () => {
      this.appliedDate = this.state.date
    })
    this.afterButtonPressed()

    if (typeof this.props.onApplyClick === 'function') {
      this.props.onApplyClick(this.state.date, this.state.dateString)
    }
  }

  onBlur = (e: any) => {
    this.refreshDate().then(() => {

      if (typeof this.props.onBlur === 'function') {
        this.props.onBlur(e)
      }

      this.closePicker()
    })
  }

  render () {
    const value: any = this.state.date
    const { children, wrapperClassName, ...customProps } = {
      ...this.props,

      format: this.format,

      open: this.state.open,

      onChange: this.onChange,

      onOpenChange: this.onOpenChange,

      onBlur: this.onBlur,

      transitionName: 'fade', // style/core/motion/fade.less; default zoom-big

      value: value,

      className: classNames(this.props.className, 'v-date-picker'),

      dropdownClassName: classNames(this.props.dropdownClassName, 'v-calendar-picker-container'),

      wrapperClassName: classNames(
        this.props.wrapperClassName,
        'v-date-picker-wrapper',
        {
          open: this.state.open
        }
      )
    }

    const resetClassName = classNames(
      'v-range-picker-reset',
      {
        'hidden': (this.state.date[0] == null) && (this.state.date[1] == null)
      }
    )

    const extraFooter = () => (
      <div className='v-range-picker-extra-footer'>
        <Button type='primary' onMouseDown={this.onApplyClick}>Apply</Button>
        <Button onMouseDown={this.onCancelClick}>Cancel</Button>
        <span className={resetClassName} onMouseDown={this.onResetClick}>Reset</span>
      </div>
    )

    const Children = () => {
      let childrenProps = { ...children.props }

      for (const key of Object.keys(childrenProps)) {
        if (childrenProps[key] === this.innerTextMask) {
          childrenProps[key] = this.state.innerText
        }
      }

      return React.cloneElement(children, { onMouseDown: this.showPicker, ...childrenProps })
    }

    return (
      <div className={wrapperClassName}>
        <div>
          <Children />
        </div>
        <AntRangePicker {...customProps} renderExtraFooter={extraFooter} />
      </div>
    )
  }
}
