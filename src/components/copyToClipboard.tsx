import * as React from 'react'
import Icon from './icon'
// https://github.com/sudodoki/copy-to-clipboard#readme
import copy from 'copy-to-clipboard'
import classNames from 'classnames'

interface CopyToClipboardProps {
  message: string
  className?: string
  children?: any
  icon?: string
  onClick?: () => void
}

class CopyToClipboard extends React.Component<CopyToClipboardProps, any> {
  constructor (props: any) {
    super(props)
  }

  copy = () => {
    copy(this.props.message)

    if (typeof this.props.onClick === 'function') {
      this.props.onClick()
    }
  }

  render () {
    const customProps = {
      ...this.props,

      onClick: this.copy,

      className: classNames(this.props.className, 'v-copy-to-clipboard')
    }

    return (
      <span {...customProps}>
        <Icon type='copy'/>
      </span>
    )
  }
}

export default CopyToClipboard
