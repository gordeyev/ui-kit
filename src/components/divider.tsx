import * as React from 'react'
import classNames from 'classnames'
import AntDivider from 'antd/es/divider'

interface DividerProps {
  className?: string
}

export default class Divider extends React.Component<DividerProps, any> {
  constructor (props: any) {
    super(props)
  }

  render () {
    const customClassNames = {
      ...this.props,
      className: classNames(this.props.className, 'v-divider')
    }

    return (
      <AntDivider {...customClassNames} />
    )
  }
}
