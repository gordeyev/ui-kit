import * as React from 'react'
import { Row } from 'antd'
import classNames from 'classnames'

type Props = {
  children: React.ReactChildren,
  gutter: number,
  onClick?: (event: React.MouseEvent) => any,
  className?: string
}

export default function ListItem (props: Props) {
  const itemClasses = classNames('list-item', props.className)
  return (
    <div className={itemClasses} onClick={props.onClick}>
      <Row gutter={props.gutter}>
        {props.children}
      </Row>
    </div>
  )
}
