import * as React from 'react'
import { Col } from 'antd'
import classNames from 'classnames'

type Props = {
  children: React.ReactChildren,
  className?: string,
  xxl?: number,
  xl?: number,
  lg?: number,
  md?: number
}

export default function ListCol (props: Props) {
  const { children, className, ...restProps } = props
  const colClasses = classNames('list-item-col', 'col-with-data', className)
  return (
    <Col className={colClasses} {...restProps}>{children}</Col>
  )
}
