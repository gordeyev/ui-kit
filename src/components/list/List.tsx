import * as React from 'react'
import classNames from 'classnames'

type Props = {
  children: React.ReactChildren,
  className?: string
}

export default function List (props: Props) {
  const listClasses = classNames('list-wrapper', props.className)
  return (
    <div className={listClasses}>
      {props.children}
    </div>
  )
}
