import * as React from 'react'
import classNames from 'classnames'

// @ts-ignore
const anyColor = require.context('../icons/any_color', false, /.*\.svg$/)
// @ts-ignore
const multiColor = require.context('../icons/multi_color', false, /.*\.svg$/)

anyColor.keys().forEach(anyColor)
multiColor.keys().forEach(multiColor)

const Icon = (props: any) => (
  <i className={classNames(props.className, 'v-icon')}>
    <svg>
      <use xlinkHref={ `#${ props.type }` } />
    </svg>
  </i>
)

export default Icon
