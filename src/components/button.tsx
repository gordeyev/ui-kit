import * as React from 'react'
import AntButton from 'antd/es/button'
import Icon from './icon'
import classNames from 'classnames'

interface ButtonProps {
  className?: string
  wrapperClassName?: string
  children?: any
  icon?: string
  onClick?: any
  onMouseDown?: any
  type?: string
}

class Button extends React.Component<ButtonProps, any> {
  static Mini: any

  constructor (props: any) {
    super(props)
  }

  render () {
    const { children } = this.props

    const { icon, ...customProps }: any = {
      ...this.props,
      className: classNames(
        this.props.className,
        {
          'ant-btn-icon-only': (this.props.children == null) && (this.props.icon != null)
        }
      )
    }

    return (
      <span className={classNames(this.props.wrapperClassName, 'v-btn-wrapper')}>
        <AntButton {...customProps}>
          {icon &&
            <Icon type={icon} />
          }
          {children &&
            <span>{children}</span>
          }
        </AntButton>
      </span>
    )
  }
}

class Mini extends React.Component<ButtonProps, any> {
  constructor (props: any) {
    super(props)
  }

  render () {
    let customProps = {
      ...this.props,
      className: 'v-btn-mini'
    }

    return (
      <Button {...customProps} />
    )
  }
}

Button.Mini = Mini

export default Button
