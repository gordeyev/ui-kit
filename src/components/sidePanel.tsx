import * as React from 'react'
import classNames from 'classnames'
import AntModal from 'antd/es/modal'
import Icon from './icon'

interface SidePanelProps {
  className?: string
  theme?: string
  mainTitle: string
  subTitle: string
  icon?: string
  onCancel?: any
}

export default class SidePanel extends React.Component<SidePanelProps, any> {
  constructor (props: any) {
    super(props)
  }

  onXClick = () => {
    this.props.onCancel()
  }

  getHeader = () => {
    let icon = this.props.icon || 't-undefined'
    return (
      <div>
        <div className='v-modal-close' onClick={this.onXClick}>
          <span className='v-modal-close-x'><Icon type='close' /></span>
        </div>
        <div className='v-icon-wrapper'>
          <Icon type={icon} />
        </div>
        <div className='v-titles-wrapper'>
          <h1>{this.props.mainTitle}</h1>
          <p>{this.props.subTitle}</p>
        </div>
      </div>
    )
  }

  render () {
    const customProps = {
      ...this.props,

      footer: null,

      title: this.getHeader(),

      width: 'auto',

      transitionName: 'slide-fade',

      closable: false,

      className: classNames(
        this.props.className,
        'v-side-panel',
        {
          // theme support
          [`v-side-panel-${this.props.theme}`]: this.props.theme != null,
          'v-side-panel-black': (this.props.theme == null) || (this.props.theme === 'black')
        }
      )
    }
    return (
      <AntModal {...customProps} />
    )
  }
}
