import React from 'react'
import ContextMenu from '../contextMenu'
import Radio from '../radio'

interface ContextMenuRadioProps {
  data: any
  children: any
  placement: any
  onChange?: (value: any) => any
}

export default class ContextMenuRadio extends React.Component<ContextMenuRadioProps, any> {
  state = {
    visible: false,
    value: this.props.data.value
  }

  onChange = (value: any) => {
    if (typeof this.props.onChange === 'function') {
      this.props.onChange(value)
    }
    this.setState({
      value: value
    }, () => {
      this.setState({
        visible: false
      })
    })
  }

  handleVisibleChange = (visible: boolean) => {
    this.setState({
      visible: visible
    })
  }

  render () {
    const content = (
      <div>
        {this.props.data.lists.map((radioGroup: any, index: number) => {
          const contextMenuProps = {
            title: radioGroup.title,
            key: index
          }

          const radioGroupProps = {
            onChange: (e: any) => { this.onChange(e.target.value) },
            value: this.state.value
          }

          return (
            <ContextMenu.Block {...contextMenuProps}>
              <Radio.Group {...radioGroupProps}>
                {radioGroup.options.map((radio: any) => {
                  const radioProps = {
                    value: radio.value,
                    key: radio.value
                  }

                  return (<Radio {...radioProps}>{radio.text}</Radio>)
                })}
              </Radio.Group>
            </ContextMenu.Block>
          )
        })}
      </div>
    )

    const customProps = {
      ...this.props,
      content: content,
      visible: this.state.visible,
      onVisibleChange: this.handleVisibleChange
    }

    return (
      <ContextMenu {...customProps} />
    )
  }
}
