import React from 'react'
import Row from 'antd/es/row'
import ContextMenu from '../contextMenu'
import Checkbox from '../checkbox'

interface ContextMenuCheckboxesProps {
  data: any
  children: any
  placement: any
  checkedList?: Array<any>
  onChange?: (values: any) => any
  onCancel?: () => any
}

const { Block } = ContextMenu

export default class ContextMenuCheckboxes extends React.Component<ContextMenuCheckboxesProps, any> {
  state = {
    visible: false,
    values: [],
    checkAll: undefined,
    indeterminate: undefined,
    checkedList: []
  }

  appliedCheckedList = []

  constructor (props: any) {
    super(props)
    const options = this.props.data.options
    const resultCheckedList: any = []
    const checkedList: any = this.props.checkedList
    for (const val of Object.keys(options)) {
      if (checkedList.includes(val)) {
        resultCheckedList.push(options[val].text)
      }
    }

    this.state.checkedList = resultCheckedList
    this.appliedCheckedList = resultCheckedList
  }

  getOptionList = () => {
    const options = this.props.data.options
    let result = []
    for (const value of Object.keys(options)) {
      result.push(options[value].text)
    }
    return result
  }

  onCheckChange = (checkedList: any) => {
    this.setState({
      checkedList,
      indeterminate: !!checkedList.length && (checkedList.length < this.getOptionList().length),
      checkAll: checkedList.length === this.getOptionList().length
    })
  }

  onCheckAllChange = (e: any) => {
    this.setState({
      checkedList: e.target.checked ? this.getOptionList() : [],
      indeterminate: false,
      checkAll: e.target.checked
    })
  }

  getValues = () => {
    const options = this.props.data.options
    const checkedList: any = this.state.checkedList
    let values: any = []

    for (const value of Object.keys(options)) {
      if (checkedList.includes(options[value].text)) {
        values.push(value)
      }
    }

    return values
  }

  handleVisibleChange = (visible: boolean) => {
    this.setState({
      visible: visible
    }, () => {
      this.setState({
        checkedList: this.appliedCheckedList
      })
    })
  }

  onApply = () => {
    const values: any = this.getValues()
    this.appliedCheckedList = this.state.checkedList
    this.handleVisibleChange(false)
    if (typeof this.props.onChange === 'function') {
      this.props.onChange(values)
    }
  }

  onCancel = () => {
    this.handleVisibleChange(false)
    if (typeof this.props.onCancel === 'function') {
      this.props.onCancel()
    }
  }

  getContent = () => {
    const customProps = {
      indeterminate: this.state.indeterminate,
      onChange: this.onCheckAllChange,
      checked: this.state.checkAll
    }

    return (
      <div>
        <Block>
          <Row>
            <Checkbox {...customProps}>
              Все разрешения
            </Checkbox>
          </Row>
          <Row>
            <Checkbox.Group options={this.getOptionList()} value={this.state.checkedList} onChange={this.onCheckChange} />
          </Row>
        </Block>
      </div>
    )
  }

  render () {
    const customProps = {
      ...this.props,
      content: this.getContent(),
      visible: this.state.visible,
      hasFooter: true,
      onVisibleChange: this.handleVisibleChange,
      onApply: this.onApply,
      onCancel: this.onCancel
    }

    return (
      <ContextMenu {...customProps} />
    )
  }
}
