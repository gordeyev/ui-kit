import React from 'react'
import ContextMenu from '../contextMenu'
import Button from '../button'

interface ContextMenuButtonsProps {
  data: Array<any>
  children: any
  placement: any
  onChange?: (value: any) => any
}

export default class ContextMenuButtons extends React.Component<ContextMenuButtonsProps, any> {
  state = {
    visible: false
  }

  onChange = (value: any) => {
    if (typeof this.props.onChange === 'function') {
      this.props.onChange(value)
    }
    this.setState({
      visible: false
    })
  }

  handleVisibleChange = (visible: boolean) => {
    this.setState({
      visible: visible
    })
  }

  render () {
    const content = (
      <ContextMenu.Block>
        {this.props.data.map((btn) =>
          <Button key={btn.value} onMouseDown={() => this.onChange(btn.value)}>{btn.text}</Button>
        )}
      </ContextMenu.Block>
    )

    const customProps = {
      ...this.props,
      content: content,
      visible: this.state.visible,
      onVisibleChange: this.handleVisibleChange
    }

    return (
      <ContextMenu {...customProps} />
    )
  }
}
