import * as React from 'react'
import AntSpin from 'antd/es/spin'

export default class Spin extends React.Component {
  constructor (props: any) {
    super(props)
  }
  render () {
    return (
      <AntSpin {...this.props} />
    )
  }
}
