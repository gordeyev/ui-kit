import * as React from 'react'
import classNames from 'classnames'
import AntMenu from 'antd/es/menu'

interface MenuProps {
  className?: string
  wrapperClassName?: string
}

// variable for animation
const menuItemHeight = 48

// it is for selected menu item
let getLinePosition = function (menuComponentProps: any) {
  let key = menuComponentProps.defaultSelectedKeys[0]
  let children = menuComponentProps.children
  let getIndex = function () {
    let result = 0
    for (let i = 0; i < children.length; i++) {
      let child = children[i]
      if (child.key === key) {
        result = i
      }
    }
    return result
  }
  let index = getIndex()
  return (index + 0.5) * menuItemHeight
}

export default class Menu extends React.Component<MenuProps, any> {
  static Divider = AntMenu.Divider
  static Item = AntMenu.Item
  static SubMenu = AntMenu.SubMenu
  static ItemGroup = AntMenu.ItemGroup

  constructor (props: any) {
    super(props)
  }

  render () {
    const { wrapperClassName, ...customProps } = {
      ...this.props,
      wrapperClassName: classNames(this.props.wrapperClassName, 'v-menu-wrapper')
    }

    return (
      <div className={wrapperClassName}>
        <div className='v-menu-left-side'>
          <div className='v-selected-menu-line' style={{ transform: `translate3d(0, ${getLinePosition(this.props)}px, 0)` }} />
        </div>
        <AntMenu {...customProps} />
      </div>
    )
  }
}
