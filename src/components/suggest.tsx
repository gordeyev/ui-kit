import * as React from 'react'
import classNames from 'classnames'
import Input from './input'
import ContextMenu from './contextMenu'
import Button from './button'

interface SuggestProps {
  overlayClassName?: string
  data?: any
  onChange?: (value?: string) => any
  onPressEnter?: (value?: string) => any
  onKeyDown?: (e: any) => any
  value?: string
  overlayStyle?: object
  autoComplete: boolean
  onSuggestClick?: (data: any) => any
  onSuggestSelected?: (data: any) => any
  noMatchesText?: string
  onClear?: () => any
}

export default class Suggest extends React.Component<SuggestProps, any> {
  trigger: any = 'focus'

  state = {
    value: this.props.value || '',
    overlayWidth: 'auto',
    arrowControlSelectedIndex: -1
  }

  arrowControl = {
    suggestLength: 0,
    currentItem: {
      text: ''
    }
  }

  input: any = undefined

  constructor (props: any) {
    super(props)
    this.input = React.createRef()
  }

  getInputWidth = () => {
    let width = 'auto'
    if (this.input) {
      width = `${this.input.current.width()}px`
    }
    return width
  }

  onMouseDown = (value: any) => {
    this.onChange(value)
  }

  onPressEnter = () => {
    if (this.state.arrowControlSelectedIndex !== -1) {
      if (this.props.autoComplete) {
        this.setState({
          value: this.arrowControl.currentItem.text
        }, this.input.current.blur)
      } else {
        this.input.current.blur()
      }
    }

    if (this.props.onSuggestSelected) {
      this.props.onSuggestSelected({ item: this.arrowControl.currentItem, value: this.state.value })
    }

    if (typeof this.props.onPressEnter === 'function') {
      this.props.onPressEnter(this.state.value)
    }
  }

  onKeyDown = (e: any) => {
    switch (e.keyCode) {
      case 38:
        this.onPressUpArrow()
        break
      case 40:
        this.onPressDownArrow()
        break
    }

    if (typeof this.props.onKeyDown === 'function') {
      this.props.onKeyDown(e)
    }
  }

  getNewPosition = (d: number) => {
    return (this.state.arrowControlSelectedIndex + this.arrowControl.suggestLength + d) % this.arrowControl.suggestLength
  }

  onPressUpArrow = () => {
    this.setState({
      arrowControlSelectedIndex: this.getNewPosition(-1)
    })
  }

  onPressDownArrow = () => {
    this.setState({
      arrowControlSelectedIndex: this.getNewPosition(1)
    })
  }

  onChange = (value: string) => {
    this.setState({
      value: value,
      arrowControlSelectedIndex: -1
    }, () => {
      if (typeof this.props.onChange === 'function') {
        this.props.onChange(this.state.value)
      }
    })
  }

  onClear = () => {
    if (typeof this.props.onClear === 'function') {
      this.props.onClear()
    }
  }

  onInputChange = (value: string) => {
    this.onChange(value)
  }

  onVisibleChange = () => {
    this.setState({
      overlayWidth: this.getInputWidth()
    })
  }

  filterData = (data: any, autoComplete = true) => {
    let suggestLength = 0

    const q: string = this.state.value.toLowerCase()
    let result: Array<any> = []

    data.map((subList: any) => {
      let options: Array<any> = []

      subList.options.map((item: any) => {
        if (autoComplete) {
          if (item.text.toLowerCase().includes(q)) {
            options.push(item)
          }
        } else {
          options.push(item)
        }
      })

      if (options.length > 0) {
        suggestLength += options.length
        result.push({
          title: subList.title,
          options: options
        })
      }
    })

    this.arrowControl.suggestLength = suggestLength

    return result
  }

  render () {
    const { autoComplete, onSuggestSelected, onSuggestClick, noMatchesText, ...otherProps } = {
      ...this.props,
      noMatchesText: this.props.noMatchesText || 'Нет совпадений',
      onSuggestClick: this.props.onSuggestClick,
      onSuggestSelected: this.props.onSuggestSelected
    }

    const filteredData = this.filterData(this.props.data, autoComplete)

    let btnIndex = -1

    const content = (
      <div>
        {filteredData.length ?
          filteredData.map((subList: any, index1: number) =>

            <ContextMenu.Block title={subList.title} key={index1}>
              {subList.options.map((item: any) => {
                btnIndex += 1
                let isBtnSelected = btnIndex === this.state.arrowControlSelectedIndex

                if (isBtnSelected) {
                  this.arrowControl.currentItem = item
                }

                const btnProps = {
                  key: btnIndex,
                  onMouseDown: () => {
                    if (onSuggestSelected) {
                      onSuggestSelected({ item: item, value: this.state.value })
                    }
                    if (onSuggestClick) {
                      onSuggestClick({ item: item, value: this.state.value })
                    }
                    if (autoComplete) {
                      this.onChange(item.text)
                    }
                  },
                  className: classNames({ 'v-btn-selected': isBtnSelected })
                }

                return (<Button {...btnProps}>{item.text}</Button>)
              })}
            </ContextMenu.Block>

          ) : <div className='v-suggest-empty'>{noMatchesText}</div>
        }
      </div>
    )

    const contextMenuProps = {
      content: content,
      trigger: this.trigger,
      overlayClassName: classNames(this.props.overlayClassName, 'v-suggest'),
      overlayStyle: { ...this.props.overlayStyle, width: this.state.overlayWidth },
      onVisibleChange: this.onVisibleChange,
      placement: 'bottomLeft'
    }

    const inputProps = {
      ...otherProps,
      ref: this.input,
      onChange: this.onInputChange,
      onPressEnter: this.onPressEnter,
      onKeyDown: this.onKeyDown,
      onClear: this.onClear,
      value: this.state.value
    }

    return (
      <ContextMenu {...contextMenuProps}>
        <Input {...inputProps} />
      </ContextMenu>
    )
  }
}
