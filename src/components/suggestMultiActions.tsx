import * as React from 'react'
import Suggest from './suggest'

interface SuggestMultiActionsProps {
  onSuggestSelected?: (data: any) => any
}

export default class SuggestMultiActions extends React.Component<SuggestMultiActionsProps, any> {
  onSuggestSelected = (data: any) => {
    if (typeof this.props.onSuggestSelected === 'function') {
      this.props.onSuggestSelected(data)
    }
  }

  render () {
    const customProps = {
      ...this.props,
      autoComplete: false,
      onSuggestSelected: this.onSuggestSelected
    }

    return (
      <Suggest {...customProps} />
    )
  }
}
