import * as React from 'react'
import classNames from 'classnames'
import AntBadge from 'antd/es/badge'

interface ButtonProps {
  className?: string
  count?: number
}

export default class Badge extends React.Component<ButtonProps, any> {
  state = {
    count: 0
  }

  constructor (props: any) {
    super(props)
    if (this.props.count) {
      this.state.count = this.props.count >= 99 ? 99 : this.props.count
    }
  }

  render () {
    const customProps = {
      ...this.props,

      count: this.state.count,

      className: classNames(this.props.className, 'v-badge')
    }

    return (
      <AntBadge {...customProps} />
    )
  }
}
