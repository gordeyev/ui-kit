import * as React from 'react'
import classNames from 'classnames'
import AntRadio from 'antd/es/radio'

interface RadioProps {
  className?: string
}

interface RadioGroupProps {
  className?: string
}

class Radio extends React.Component<RadioProps, any> {
  static Group: any

  constructor (props: any) {
    super(props)
  }

  render () {
    const customProps = {
      ...this.props,
      className: classNames(this.props.className, 'v-radio-wrapper')
    }

    return <AntRadio {...customProps} />
  }
}

const Group = function (props: RadioGroupProps): any {
  const customProps = {
    ...props,
    className: classNames(props.className, 'v-radio-group')
  }

  return (
    <AntRadio.Group {...customProps} />
  )
}

Radio.Group = Group

export default Radio
