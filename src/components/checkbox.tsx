import * as React from 'react'
import classNames from 'classnames'
import AntCheckbox from 'antd/es/checkbox'

interface CheckboxProps {
  className?: string
}

class Checkbox extends React.Component<CheckboxProps, any> {
  static Group: any

  constructor (props: any) {
    super(props)
  }

  render () {
    const customProps = {
      ...this.props,

      children: this.props.children,

      className: classNames(this.props.className, 'v-single-checkbox')
    }
    return (
      <AntCheckbox {...customProps} />
    )
  }
}

const Group = function (props: any): any {
  const customProps = {
    ...props,
    className: classNames(props.className, 'v-checkbox-group')
  }
  return (
    <AntCheckbox.Group {...customProps} />
  )
}

Checkbox.Group = Group

export default Checkbox
