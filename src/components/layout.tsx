import * as React from 'react'
import classNames from 'classnames'
import AntLayout from 'antd/es/layout'

interface LayoutProps {
  className?: string
  ismainlayout?: 'true' | 'false'
}

interface SiderProps {}

const AntSider = AntLayout.Sider

class Sider extends React.Component<SiderProps, any> {
  width = '268'

  constructor (props: any) {
    super(props)
  }

  render () {
    const customProps = {
      ...this.props,
      width: this.width
    }

    return <AntLayout.Sider {...customProps} />
  }
}

export default class Layout extends React.Component<LayoutProps, any> {
  static Header: any = AntLayout.Header
  static Footer: any = AntLayout.Footer
  static Content: any = AntLayout.Content
  static Sider: any = Sider

  constructor (props: any) {
    super(props)
  }

  render () {
    const customProps = {
      ...this.props,
      className: classNames(
        this.props.className,
        'v-layout',
        {
          'v-main-layout': this.props.ismainlayout === 'true'
        }
      )
    }

    return (
      <AntLayout {...customProps} />
    )
  }
}
