import * as React from 'react'
import classNames from 'classnames'
import AntTable from 'antd/es/table'

interface SimpleTableProps {
  wrapperClassName?: string
}

class SimpleTable extends React.Component<SimpleTableProps, any> {
  pagination: any = false

  dataSource: Array<any> = []

  columns: Array<any> = []

  constructor (props: any) {
    super(props)
    let colWidth = 'auto'
    if (props.equalWidthCols) {
      colWidth = `${100 / props.colsNumber}%`
    }
    for (let i = 0; i < props.data.length; i++) {
      let rowIndex = Math.floor((i) / props.colsNumber)
      let cellData = props.data[i]
      if (i < props.colsNumber) {
        this.columns.push({
          title: '',
          dataIndex: `column${i + 1}`,
          width: colWidth,
          key: `column${i + 1}`,
          render: function (colData: any) {
            if (colData !== undefined) {
              return props.render(colData)
            } else {
              return ''
            }
          }
        })
      }
      if (!this.dataSource[rowIndex]) {
        this.dataSource[rowIndex] = {
          key: `${rowIndex + 1}`
        }
      }
      this.dataSource[rowIndex][`column${(i) % props.colsNumber + 1}`] = cellData
    }
  }

  render () {
    const { wrapperClassName, ...customProps } = {
      ...this.props,

      pagination: this.pagination,

      bordered: false,

      showHeader: false,

      dataSource: this.dataSource,

      columns: this.columns,

      wrapperClassName: classNames(this.props.wrapperClassName, 'v-simple-table')
    }

    return (
      <div className={wrapperClassName}>
        <AntTable {...customProps} />
      </div>
    )
  }
}

export default SimpleTable
