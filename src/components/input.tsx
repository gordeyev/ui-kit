import classNames from 'classnames'
import * as React from 'react'
import AntInput from 'antd/es/input'
import Icon from './icon'
import Spin from './spin'

interface InputProps {
  value?: string
  placeholder?: string
  onFocus?: (e: any) => any
  onBlur?: (e: any) => any
  onKeyDown?: (e: any) => any
  onChange?: (value: string) => any
  textInput?: any
  className?: string
  wrapperClassName?: string
  spinning?: boolean
  ref?: any
  readOnly?: boolean
  onClear?: () => any
}

export default class Input extends React.Component<InputProps, any> {
  state = {
    value: '',
    focused: false
  }

  textInput: any = null

  keepCurFocus = false

  constructor (props: any) {
    super(props)
    this.textInput = React.createRef()
    this.state.value = props.value || ''
  }

  width () {
    let width = this.textInput.current.input.offsetWidth
    return width
  }

  setValue = (value: string) => {
    return new Promise((resolve) => {
      this.setState({
        value: value
      }, () => {
        resolve(value)
      })
    })
  }

  onChange = (value: string) => {
    this.setValue(value)
    if (typeof this.props.onChange === 'function') {
      this.props.onChange(value)
    }
  }

  onFocus = (e: any) => {
    this.setState({
      focused: true
    })
    setTimeout(() => {
      this.textInput.current.input.select()
    }, 100)
    if (typeof this.props.onFocus === 'function') {
      this.props.onFocus(e)
    }
  }

  onBlur = (e: any) => {
    this.setState({
      focused: false
    })
    if (this.keepCurFocus) {
      setTimeout(() => {
        this.textInput.current.input.focus()
      }, 0)
    }
    this.keepCurFocus = false
    if (typeof this.props.onBlur === 'function') {
      this.props.onBlur(e)
    }
  }

  onKeyDown = (e: any) => {
    if (e.keyCode === 27) {
      this.blur()
    }

    if (typeof this.props.onKeyDown === 'function') {
      this.props.onKeyDown(e)
    }
  }

  clearInput = () => {
    if (this.state.focused) {
      this.keepCurFocus = true
    }
    this.setValue('').then((val: string) => {
      this.onChange(val)
    })

    if (typeof this.props.onClear === 'function') {
      this.props.onClear()
    }
  }

  blur = () => {
    this.textInput.current.blur()
  }

  render () {
    const value = this.props.value || this.state.value

    const { spinning, onClear, ...otherProps } = {
      ...this.props,
      onClear: this.props.onClear
    }

    const inputProps = {
      ...otherProps,

      value: value,

      onFocus: this.onFocus,

      onBlur: this.onBlur,

      onKeyDown: this.onKeyDown,

      onChange: (e: any) => this.onChange(e.currentTarget.value),

      className: classNames(this.props.className, 'v-input'),

      ref: this.textInput
    }

    const wrapperClassName = classNames(
      this.props.wrapperClassName,
      'v-input-wrapper',
      {
        'focused': this.state.focused,
        'spinning': spinning,
        'has-value': value.length > 0
      }
    )

    return (
      <span className={wrapperClassName}>
        <AntInput {...inputProps} />
        {spinning &&
          <span className='v-input-spinner'>
            <Spin />
          </span>
        }
        <span className='v-clear-input' onMouseDown={this.clearInput}>
          <Icon type='close' />
        </span>
      </span>
    )
  }
}
