import * as React from 'react'
import classNames from 'classnames'
import Input from './input'
import ContextMenu from './contextMenu'

interface InputOrRangeProps {
  message: string
  className?: string
  overlayClassName?: string
  children: any
  icon?: string
  placeholder?: string
  value: string | number
  onChange?: (e: string | Array<string>) => void
}

export default class InputOrRange extends React.Component<InputOrRangeProps, any> {
  constructor (props: any) {
    super(props)
    this.state = {
      value: this.props.value || ''
    }
  }

  onChange = (val: string): void => {
    this.setState({ value: val })

    if (typeof this.props.onChange === 'function') {
      this.props.onChange(this.getValue(val))
    }
  }

  getValue (val: string): string | Array<string> {
    if (val.indexOf('-') !== -1) {
      return val.split('-')
    }

    return val
  }

  render () {
    const customInputProps = {
      value: this.state.value,

      onChange: this.onChange,

      placeholder: this.props.placeholder || 'Input number or range with "-" delimiter'
    }

    const { className, ...customProps } = {
      ...this.props,

      content: <div><Input {...customInputProps} /></div>,

      placement: 'bottomLeft',

      className: classNames(this.props.className, 'v-input-number-or-range'),

      overlayClassName: classNames(this.props.overlayClassName, 'v-input-number-or-range-tooltip')
    }

    return (
      <div className={className}>
        <ContextMenu {...customProps} />
      </div>
    )
  }
}
